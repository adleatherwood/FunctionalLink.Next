using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xunit;

#pragma warning disable 0618

namespace FunctionalLink.Next.Tests;

public enum JsonType
{
    Newtonsoft,
    SystemText
}

public class JsonTests
{
    private static JObject Serialize(JsonType type, object o)
    {
        var json = type == JsonType.Newtonsoft
            ? JsonConvert.SerializeObject(o)
            : System.Text.Json.JsonSerializer.Serialize(o);

        var doc = JObject.Parse(json);
        return doc;
    }

    private static T Deserialize<T>(JsonType type, string json)
    {
        var o = type == JsonType.Newtonsoft
            ? JsonConvert.DeserializeObject<T>(json)
            : System.Text.Json.JsonSerializer.Deserialize<T>(json);

        return o!;
    }

    [Theory]
    [InlineData(JsonType.Newtonsoft)]
    [InlineData(JsonType.SystemText)]
    public void ResultJsonSerializationOnSuccess(JsonType type)
    {
        var result = Result.Success(1);
        var actual = Serialize(type, result);
        
        Assert.Equal(result.IsSuccess, actual["IsSuccess"] );
        Assert.Equal(result.Value, actual["Value"]);
        Assert.Null(result.Error);

        var actual2 = Deserialize<Result<int>>(type, actual.ToString());

        Assert.Equal(result.IsSuccess, actual2.IsSuccess);
        Assert.Equal(result.Value, actual2.Value);
        Assert.Null(actual2.Error);
    }

    [Theory]
    [InlineData(JsonType.Newtonsoft)]
    [InlineData(JsonType.SystemText)]
    public void ResultJsonSerializationOnFailure(JsonType type)
    {
        var result = Result<int>.Failure("test");
        var actual = Serialize(type, result);
        
        Assert.Equal(result.IsSuccess, actual["IsSuccess"] );
        Assert.Equal(0, result.Value);
        Assert.Equal(result.Error!.Message, actual["Error"]!["Message"]);

        var actual2 = Deserialize<Result<int>>(type, actual.ToString());

        Assert.Equal(result.IsSuccess, actual2.IsSuccess);
        Assert.Equal(0, actual2.Value);
        Assert.Equal(result.Error.Message, actual2.Error!.Message);
    }

    [Theory]
    [InlineData(JsonType.Newtonsoft)]
    [InlineData(JsonType.SystemText)]
    public void ResultJsonSerializationOnException(JsonType type)
    {
        var result = Result<int>.Failure(new Exception("test"));
        var actual = Serialize(type, result);
        
        Assert.Equal(result.IsSuccess, actual["IsSuccess"] );
        Assert.Equal(default, result.Value);
        Assert.Equal(result.Error!.Message, actual["Error"]!["Message"]);

        var actual2 = Deserialize<Result<int>>(type, actual.ToString());

        Assert.Equal(result.IsSuccess, actual2.IsSuccess);
        Assert.Equal(default, actual2.Value);
        Assert.Equal(result.Error!.Message, actual2.Error!.Message);
    }

    [Theory]
    [InlineData(JsonType.Newtonsoft)]
    [InlineData(JsonType.SystemText)]
    public void ResultJsonSerializationOnThrownException(JsonType type)
    {
        var result = Result<int>.Failure(Throw("test"))
            .RemoveException(); 
            // system.Text.Json does not support serializing exceptions

        var actual = Serialize(type, result);
        
        Assert.Equal(result.IsSuccess, actual["IsSuccess"]);
        Assert.Equal(default, result.Value);
        Assert.Equal(result.Error!.Message, actual["Error"]!["Message"]);

        var actual2 = Deserialize<Result<int>>(type, actual.ToString());

        Assert.Equal(result.IsSuccess, actual2.IsSuccess);
        Assert.Equal(default, actual2.Value);
        Assert.Equal(result.Error!.Message, actual2.Error!.Message);
    }

    [Theory]
    [InlineData(JsonType.Newtonsoft)]
    [InlineData(JsonType.SystemText)]
    public void OptionJsonSerializationOnSome(JsonType type)
    {
        var result = Option.Some(1);
        var actual = Serialize(type, result);
        
        Assert.Equal(result.IsSome, actual["IsSome"] );
        Assert.Equal(result.Value, actual["Value"]);        

        var actual2 = Deserialize<Option<int>>(type, actual.ToString());

        Assert.Equal(result.IsSome, actual2.IsSome);
        Assert.Equal(1, actual2.Value);
    }

    [Theory]
    [InlineData(JsonType.Newtonsoft)]
    [InlineData(JsonType.SystemText)]
    public void OptionJsonSerializationOnNone(JsonType type)
    {
        var result = Option<int>.None();
        var actual = Serialize(type, result);
        
        Assert.Equal(result.IsSome, actual["IsSome"] );
        Assert.Equal(result.Value, actual["Value"]);   

        var actual2 = Deserialize<Option<int>>(type, actual.ToString());

        Assert.Equal(result.IsSome, actual2.IsSome);
        Assert.Equal(default, actual2.Value);     
    }

    [Theory]
    [InlineData(JsonType.Newtonsoft)]
    [InlineData(JsonType.SystemText)]
    public void EitherJsonSerializationOnValue(JsonType type)
    {
        var result = Either<int,string>.Value(1);
        var actual = Serialize(type, result);
        
        Assert.Equal(result.IsLeft, actual["IsLeft"] );
        Assert.Equal(result.Left, actual["Left"]);        
        Assert.Equal(result.Right, actual["Right"]);      

        var actual2 = Deserialize<Either<int,string>>(type, actual.ToString());

        Assert.Equal(result.IsLeft, actual2.IsLeft);
        Assert.Equal(result.Left, actual2.Left);        
        Assert.Equal(result.Right, actual2.Right);  
    }

    [Theory]
    [InlineData(JsonType.Newtonsoft)]
    [InlineData(JsonType.SystemText)]
    public void EitherJsonSerializationOnOther(JsonType type)
    {
        var result = Either<int,string>.Other("test");
        var actual = Serialize(type, result);
        
        Assert.Equal(result.IsLeft, actual["IsLeft"] );
        Assert.Equal(result.Left, actual["Left"]);        
        Assert.Equal(result.Right, actual["Right"]);

        var actual2 = Deserialize<Either<int,string>>(type, actual.ToString());

        Assert.Equal(result.IsLeft, actual2.IsLeft);
        Assert.Equal(result.Left, actual2.Left);        
        Assert.Equal(result.Right, actual2.Right);          
    }

    [Theory]
    [InlineData(JsonType.Newtonsoft)]
    [InlineData(JsonType.SystemText)]
    public void ErrorJsonSerialization(JsonType type)
    {
        var error = new Error("test", null);
        var actual = Serialize(type, error);
        
        Assert.Equal(error.Message, actual["Message"]);  

        var actual2 = Deserialize<Error>(type, actual.ToString());

        Assert.Equal(error.Message, actual2.Message);
    }

    private static Exception Throw(string message)
    {
        try
        {
            throw new Exception(message);
        }
        catch (Exception e)
        {
            return e;
        }
    }
}
