namespace FunctionalLink.Next;

#pragma warning disable 0618
#pragma warning disable 1591

public class Result
    : Result<None>
{
    [Obsolete(Messages.SerializationOnly)]
    public Result(bool isSuccess, Error error) 
        : base(isSuccess, None.Value, error)
    {
    }

    public static implicit operator Result(Exception failure) =>
        new Result(false, new Error(failure.Message, failure));

    public static implicit operator Result(Error failure) =>
        new Result(false, failure);

    public static Result Success() =>
        new Result(true, default!);

    public static Result<T> Success<T>(T value) =>
        Result<T>.Success(value);

    public new static Error Failure(string error) =>
        new Error(error, null);

    public new static Error Failure(Exception error) =>
        new Error(error.Message, error);
    
    public static Error Failure(string message, Exception error) =>
        new Error(message, error);

    // map
    public static Result<T> Try<T>(Func<T> f)
    {
        try
        {
            var value = f();
            return Success(value);
        }
        catch (Exception e)
        {
            return Failure(e);
        }
    }

    // map async
    public static async Task<Result<T>> Try<T>(Func<Task<T>> f)
    {
        try
        {
            var value = await f();
            return Success(value);
        }
        catch (Exception e)
        {
            return Failure(e);
        }
    }

    // bind
    public static Result<T> Try<T>(Func<Result<T>> f)
    {
        try
        {
            var value = f();
            return value;
        }
        catch (Exception e)
        {
            return Failure(e);
        }
    }

    // bind async
    public static async Task<Result<T>> Try<T>(Func<Task<Result<T>>> f)
    {
        try
        {
            var value = await f();
            return value;
        }
        catch (Exception e)
        {
            return Failure(e);
        }
    }

    // void
    public static Result<None> Try(Action f)
    {
        try
        {
            f();
            return Success();
        }
        catch (Exception e)
        {
            return Failure(e);
        }
    }

    // void async
    public static async Task<Result<None>> Try(Func<Task> f)
    {
        try
        {
            await f();
            return Success();
        }
        catch (Exception e)
        {
            return Failure(e);
        }
    }
}
