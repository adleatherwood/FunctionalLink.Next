namespace FunctionalLink.Next;

#pragma warning disable 1591

public static class Messages
{
    public const string SerializationOnly = "Do not use.  This is exposed for serialization only.";
}
