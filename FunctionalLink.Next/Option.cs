using System.Diagnostics.CodeAnalysis;

namespace FunctionalLink.Next;

#pragma warning disable 1591
#pragma warning disable 1998
#pragma warning disable 0618
#pragma warning disable 8604

public class Option<T>
{
    [Obsolete(Messages.SerializationOnly)] public bool IsSome { get; private set; }
    [Obsolete(Messages.SerializationOnly)] public T Value { get; private set; }
    
    [Obsolete(Messages.SerializationOnly)]
    public Option(bool isSome, T value) =>
        (this.IsSome, this.Value) = (isSome, value);

    public static Option<T> Some(T value) =>
        new(true, value);

    public static Option<T> None() =>
        new(false, default);

    public static Option<T> Maybe(T? value) =>
        value != null ? Some(value) : None();
        
    public static implicit operator Option<T>(T value) =>
        Some(value);

    public static implicit operator Option<T>(None none) =>
        None();

    //========================================================================== Inspect

    public bool HasSome() => 
        IsSome;
    
    public bool HasNone() => 
        !IsSome;

    //========================================================================== Decompose 

    //-------------------------------------------------------------------------- Has

    public bool HasSome([NotNullWhen(true)] out T some) 
    {
        some = this.Value!;
        return IsSome;
    }

    //-------------------------------------------------------------------------- Match

    /// <summary>
    /// Match -> Void
    /// </summary>
    public void Match(Action<T> onSome, Action onNone) 
    {
        if (IsSome)
            onSome(Value);
        else
            onNone();
    }

    /// <summary>
    /// Match -> Task
    /// </summary>
    public async Task Match(Func<T,Task> onSome, Func<Task> onNone) 
    {
        if (IsSome)
            await onSome(Value);
        else
            await onNone();
    }

    /// <summary>
    /// Match -> U
    /// </summary>
    public U Match<U>(Func<T,U> onSome, Func<U> onNone) =>
        IsSome
            ? onSome(Value) 
            : onNone();

    //-------------------------------------------------------------------------- ValueOr

    public T ValueOr(T alternate) =>
        Match(
            some => some,
            () => alternate);

    //========================================================================== Compose Left

    //-------------------------------------------------------------------------- Then/Map

    /// <summary>
    /// Map/Left -> T
    /// </summary>
    public Option<U> Then<U>(Func<T,U> f) =>
        Match(
            some => Option<U>.Some(f(some)), 
            () => Option<U>.None());   

    /// <summary>
    /// Map/Left -> T Async;
    /// </summary>
    public async Task<Option<U>> Then<U>(Func<T,Task<U>> f) =>
        await Match(
            async some => Option<U>.Some(await f(some)), 
            async () => Option<U>.None());                

    //-------------------------------------------------------------------------- Then/Bind

    /// <summary>
    /// Bind/Left -> Option
    /// </summary>
    public Option<U> Then<U>(Func<T,Option<U>> f) =>
        Match(
            some => f(some).Match(
                some1 => Option<U>.Some(some1),
                () => Option<U>.None()), 
            () => Option<U>.None());

    /// <summary>
    /// Bind/Left -> Option Async
    /// </summary>
    public async Task<Option<U>> Then<U>(Func<T,Task<Option<U>>> f) =>
        await Match(
            async some => (await f(some)).Match(
                some1 => Option<U>.Some(some1),
                () => Option<U>.None()), 
            async () => Option<U>.None());

    //-------------------------------------------------------------------------- Then/Void

    /// <summary>
    /// Void/Left -> Option
    /// </summary>
    public Option<T> Then(Action<T> f)
    {
        Match(
            some => { f(some); },
            () => { });
        return this;
    }

    /// <summary>
    /// Void/Left -> Option Async
    /// </summary>
    public async Task<Option<T>> Then(Func<T, Task> f)
    {
        await Match(
            async some => { await f(some); },
            async () => { });
        return this;
    }

    //========================================================================== Compose Right

    //-------------------------------------------------------------------------- Else/Map
    
    /// <summary>
    /// Map/Right -> T
    /// </summary>
    public Option<T> Else(Func<T> f) =>
        Match(
            some => Option<T>.Some(some), 
            () => Option<T>.Some(f()));   

    /// <summary>
    /// Map/Right -> T Async
    /// </summary>
    public async Task<Option<T>> Else(Func<Task<T>> f) =>
        await Match(
            async some => Option<T>.Some(some), 
            async () => Option<T>.Some(await f()));        

    //-------------------------------------------------------------------------- Else/Bind

    /// <summary>
    /// Bind/Right -> Option
    /// </summary>
    public Option<T> Else(Func<Option<T>> f) =>
        Match(
            _ => this, 
            () => f());   

    /// <summary>
    /// Bind/Right -> Option Async
    /// </summary>
    public async Task<Option<T>> Else(Func<Task<Option<T>>> f) =>
        await Match(
            async _ => this, 
            async () => await f());        

    //-------------------------------------------------------------------------- Else/Void

    /// <summary>
    /// Void/Right -> Void
    /// </summary>
    public void Else(Action f) =>
        Match(
            _ => {}, 
            () => f());   

    /// <summary>
    /// Void/Right -> Task
    /// </summary>
    public async Task Else(Func<Task> f) =>
        await Match(
            async _ => {}, 
            async () => await f());        

    //========================================================================== Adapter Left

    //-------------------------------------------------------------------------- Filter

    public Option<T> Filter(Func<T,bool> predicate) => 
        Match(
            some => predicate(some) ? this : None(),
            () => this);
        
    //========================================================================== Combinator Left

    //-------------------------------------------------------------------------- Or

    public Option<T> Or(Option<T> other) => 
        Match(
            some => this,
            () => other);

    public Option<T> Or(Func<Option<T>> other) => 
        Match(
            some => this,
            () => other());

    public Task<Option<T>> Or(Func<Task<Option<T>>> other) => 
        Match(
            _  => Task.FromResult(this),
            () => other());

    //-------------------------------------------------------------------------- And

    public Option<U> And<TOther,U>(Option<TOther> other, Func<T,TOther, U> selector) => 
        Match(
            some => other.Match(
                some1 => Option<U>.Some(selector(some, some1)),
                () => Option<U>.None()),
            () => Option<U>.None());

    public Option<U> And<TOther,U>(Func<Option<TOther>> other, Func<T,TOther,U> selector) => 
        Match(
            some => other().Match(
                some1 => Option<U>.Some(selector(some, some1)),
                () => Option<U>.None()),
            () => Option<U>.None());

    public async Task<Option<U>> And<TOther,U>(Func<Task<Option<TOther>>> other, Func<T,TOther,U> selector) => 
        await Match(
            async some => (await other()).Match(
                some1 => Option<U>.Some(selector(some, some1)),
                () => Option<U>.None()),
            async () => Option<U>.None());
}

